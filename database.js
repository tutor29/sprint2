const mongoose = require('mongoose');

const mongodbConnection = async function(){
    try {
        const db = await mongoose.connect("mongodb://localhost:27017/sprint2");
        console.log(`Database is connected to ${db.connection.name}`,)
    } catch (error) {
        console.log(error);
    }
}

module.exports = mongodbConnection;