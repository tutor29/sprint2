const app = require('./app');
const mongodbConnection = require('./database')
const productRoutes = require('./routes/product.routes');
const categoryRoutes = require('./routes/category.routes')

// Conexión a la base de datos
mongodbConnection();

// inicializamos las rutas
app.use('/products', productRoutes);
app.use('/categories', categoryRoutes);

app.listen(3000, () => {
    console.log('Sever is running on port 3000');
})