const express = require('express');
const addCategory = require('../controllers/category.controller');

const router = new express.Router();

router.post('/', addCategory);

module.exports = router;