const express = require('express');
const { listProducts, addProduct } = require('../controllers/product.controller')

const router = express.Router();

/**
 * async (req, res) => {}
 * Funciones definidas en los controlodores
 */

router.get('/', listProducts);
router.post('/', addProduct);
// router.get('/:id', );
// router.get('/:name', );
// router.delete('/:id', );
// router.put('/:id', );

module.exports = router;