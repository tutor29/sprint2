const mongoose = require('mongoose');

const roleSchema = mongoose.Schema({
    name: String,
    description: String,
});

module.exports = mongoose.model('Role', roleSchema);