const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    name: String,
    price: Number,
    brand: String,
    stock: Number,
    category: [
        {
            ref: 'Category',
            type: mongoose.Types.ObjectId,
        }
    ]
},
{
    timestamps: true,
    versionKey: false,
});

module.exports = mongoose.model('Product', productSchema);