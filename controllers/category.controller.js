const Category = require("../models/Category")


const addCategory = async (req, res) => {
    try {
        const newCategory = new Category(req.body);
        const savedCategory = await newCategory.save();
        res.json(savedCategory);
    } catch (error) {
        res.send(error);
    }
}

module.exports = addCategory;