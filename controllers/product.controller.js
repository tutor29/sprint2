const Product = require("../models/Product");

const listProducts = async (req, res) => {
    try {
        const products = await Product.find().populate('category');
        res.json(products);
    } catch (error) {
        req.send(error);
    }
}

const addProduct = async (req, res) => {
    try{
        const newProduct = new Product(req.body);
        const savedProduct = await newProduct.save();
        res.json(savedProduct);
    }catch(error){
        res.send(error);
    }
}

module.exports = {
    listProducts,
    addProduct,
}